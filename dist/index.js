let indexed = require('../indexed');

module.exports = {
	indexed: indexed.Indexed,
	saltIndexed: indexed.SaltIndexed,
};
