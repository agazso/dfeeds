let assert = require('assert');

let indexed = require('../indexed');
let hexutil = require('../hexutil');

let topicHex = "6974776173746865626573746f6674696d6573776f7273746f6674696d65730a";
//let topic
let topic = hexutil.hexToArray(topicHex);
topic = topic.slice(0, 20);

describe('feed', () => {
	it('indexed', function() {
		let topicTooLong = new Uint8Array(34);
		let err = false;
		try {
			new indexed.Indexed(topicTooLong);
		} catch {
			err = true;
		}
		assert(err);

		let feed = new indexed.Indexed(topic);
		a = feed.next();
		b = feed.next();
		// TODO: good enough for now, but should check values:
		// <Buffer 4a 22 82 3c 92 24 66 1f ca 08 f0 40 6c d3 32 8e 3b be 01 6c ab f2 15 e7 aa d2 15 c3 b8 67 ef 64>
		// <Buffer f0 7a 43 50 f2 e6 31 1f 8e 6f 59 0a 28 1d d8 c1 a0 c9 49 ce 32 80 ce 8e 61 d8 82 ca fb 81 03 50>
		assert.notEqual(a, b);
		assert.equal(feed.index, 1n);
	});

	it('saltIndexed', function() {
		let salt = new Uint8Array(32);
		let feed = new indexed.SaltIndexed(topic, salt);
		a = feed.next();
		b = feed.next();
		assert.notEqual(a, b);
	});
});
