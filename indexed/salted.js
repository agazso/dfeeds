let keccak = require('keccak');
let hexutil = require('../hexutil');

let topicLength = 20;
let saltLength = 32;
let bigOne = BigInt(1);
let bigNone = BigInt(-1);

module.exports = {
	SaltIndexed: SaltIndexed,
};

let pad = '';
for (let i = 0; i < 64; i++) {
	pad += '0';
}

function SaltIndexed(topic, salt) {
	if (topic.length != topicLength) {
		throw 'invalid topic length (' + topic.length + ' != ' + topicLength + ')';
	}
	if (salt.length != saltLength) {
		throw 'invalid salt length (' + salt.length + ' != ' + saltLength + ')';
	}
	h = keccak('keccak256');
	h.update(Buffer.from(topic));
	h.update(Buffer.from(salt));
	this.salted = new Uint8Array(h.digest());
	this.index = BigInt(-1);
}

SaltIndexed.prototype.next = function() {
	this.index = this.index + 1n;
	let id = this.current();	
	return id;
};

SaltIndexed.prototype.skip = function(n) {
	this.index = this.index + BigInt(n);
};

SaltIndexed.prototype.current = function() {
	if (this.index == bigNone) {
		throw 'no updates made';
	}
	let indexHexRaw = pad + this.index.toString(16);
	let indexHex = indexHexRaw.slice((-1 * indexHexRaw.length) + 1);
	let indexBytes = hexutil.hexToArray(indexHex);

	h = keccak('keccak256');
	h.update(Buffer.from(this.salted));
	h.update(Buffer.from(indexBytes));

	return new Uint8Array(h.digest());
}
