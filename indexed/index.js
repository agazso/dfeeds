let indexed = require('./indexed');
let salted = require('./salted');

module.exports = {
	Indexed: indexed.Indexed,
	SaltIndexed: salted.SaltIndexed,
};
